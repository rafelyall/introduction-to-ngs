{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": "true",
    "editable": "true",
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "<header style=\"text-align:center;color:darkgray\">Introduction to Next Generation Sequencing</header>\n",
    "<header style=\"text-align:center;color:darkgray\">2021</header>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": "true",
    "editable": "true",
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "<h1>Help</h1>\n",
    "\n",
    "<p>This page contains basic information about the course - please read through it before starting. Later, you can use it as a quick reference for problems or links to the course notes for each workspace.</p>\n",
    "\n",
    "<p>The topics covered in this course may seem incredible complex for those unfamiliar with them, and you may suspect that you will forget most of these details once the course is over. This is almost definitely true. However, even just knowing that some of these topics exist in the first place could be a major help if you ever need to pick them up again as you continue your studies - at the very least you will know what search terms to use.</p>\n",
    "\n",
    "<h3>Quick tips</h3>\n",
    "\n",
    "<p>The free Binder JupyterHub server hosting this environment comes with some limits:</p>\n",
    "\n",
    "<ul>\n",
    "\t<li>Each time you launch the course Binder will create a <b>new</b> server. Any previous changes you had made or documents you created will be gone. If you need to save a document (such as a script), you can download it from the server or simply copy-paste the code somewhere else.</li>\n",
    "\t<li>After <b>~30 minutes of inactivity</b> or <b>~7 hours total uptime</b> the server will shut down automatically.</li>\n",
    "\t<li>The server is not very powerful (less so than even an old laptop or desktop), though this will not be an issue for this course.</li>\n",
    "</ul>\n",
    "\n",
    "<p>To open a terminal you can use the Open Terminal button at the top of the notebook (<img style=\"height:18px;vertical-align:-20%\" src=\"jupyterlab-workshop-extension/style/terminal.svg\">), or the shortcut <button style=\"border-style: outset\">SHIFT</button>+<button>TAB</button>.</p>\n",
    "\n",
    "<p>Drag any tab to dock it to the top/bottom/left/right of the screen. You can have multiple tabs open at once inside JupyterLab.</p>\n",
    "\n",
    "<p>Some notes (such as the Contents and Help pages) are Jupyter Notebooks. These will not be discussed in detail, but double-clicking paragraphs in these notes will convert them to Edit Mode. If you suspect this has happened, use <button>CTRL</button>+<button>ENTER</button> to reset it. You can prevent this behaviour entirely using the Lock Cells button at the top of the notebook (<img style=\"height:18px;vertical-align:-15%;opacity:0.75\" src=\"jupyterlab-workshop-extension/style/lock.svg\">). This change will reset if a notebook is closed/reopened.</p>\n",
    "\n",
    "<p>Some URL links may not open directly when clicked. Use <i>Right click -> Open link in new tab</i> to view them on a separate page.</p>\n",
    "\n",
    "<p>To search within a document use the browser search function (e.g. in Chrome, click the three dots and \"Find\"). The <button>CTRL</button>+<button>F</button> hotkey will not work.</p>\n",
    "\n",
    "<p>You can download the course notes from the JupyterLab file browser (<span class=\"path\"><i>DayX_course-notes.html</i></span>) and view them in any internet browser.</p>\n",
    "\n",
    "<h2>Links to course notes</h2>\n",
    "\n",
    "[Day 1](./Day1_course-notes.html) | [Day 2](./Day2_course-notes.html) | [Day 3](./Day3_course-notes.html) | [Day 4](./Day4_course-notes.html)  | [Day 5](./Day5_course-notes.html)\n",
    "\n",
    "<h2>General Information</h2>\n",
    "\n",
    "<h3><i>Binder/JupyterLab</i></h3>\n",
    "\n",
    "<p><b>Workspaces layout</b>: Try have only one JupyterLab workspace open at once in your browser. If you have the same workspace open in multiple tabs the layout will not be maintained (i.e. this help file and the course notes docked to the top/bottom of the screen).</p>\n",
    "\n",
    "<p><b>File browser</b>: You can open the JupyterLab File Browser from the folder icon on the left sidebar or shortcut <button>CTRL</button>+<button>SHIFT</button>+<button>F</button> to access workshop files (such as course notes) through a graphical interface.</p>\n",
    "\n",
    "<p><b>Edit Mode</b>: Press <button>CTRL</button>+<button>ENTER</button> in a cell to change it back to the normal view if you accidentally double-click it and enter Edit Mode. If you do this often you can also simply lock all cells in the notebook using the Lock Cells button at the top of the notebook (<img style=\"height:18px;vertical-align:-15%;opacity:0.75\" src=\"jupyterlab-workshop-extension/style/lock.svg\">).</p>\n",
    "\n",
    "<p><b>Relaunching the terminal</b>: If you accidentally close the terminal tab you can restore it from the \"Running Kernels and Terminals\" menu on the left of the screen (black circle containing a white square). To open a new terminal you can use the Open Terminal button at the top of the notebook (<img style=\"height:18px;vertical-align:-20%\" src=\"jupyterlab-workshop-extension/style/terminal.svg\">), or the shortcut <button>SHIFT</button>+<button>TAB</button>.</p>\n",
    "\n",
    "<p><b>JupyterLab tabs and docking</b>: JupyterLab can have multiple internal tabs open at once and you can dock up to four tabs to the top/bottom/left/right of the screen simultaneously. This is useful if you want to have two or more separate documents open at once (or multiple documents and/or terminals).</p>\n",
    "\n",
    "<h3><i>Errors</i></h3>\n",
    "\n",
    "<p><b>\"Failed to connect to event stream\"</b>: This error can occur when launching the Binder server. Simply refresh the page (F5) and the Binder launch will have continued in the background.</p>\n",
    "\n",
    "<p><b>\"File is different to one on disk\"</b>: This warning may occasionally occur if more than one tab is open at once. You can select either \"revert\" or \"overwrite\", as any changes will only last as long as the server instance in which you are working. Closing the Help file will also prevent this from occuring.</p>\n",
    "\n",
    "<p><b>\"Directory not found\"</b>: This error means the server has automatically shut down. Make sure to regularly back-up any data you wish to keep (such as scripts) to your personal computer, as changes from previous sessions will not be saved. However, the course is structured such that this should not be necessary very often.</p>\n",
    "\n",
    "<h3><i>Reading the course notes</i></h3>\n",
    "\n",
    "<p><i>It is possible that the course notes may look slightly different across browsers. If you see anything strange just ask.</i></p>\n",
    "\n",
    "<p><b>Font size:</b> You can adjust the browser zoom using the <button>CTRL</button>+<button>+</button> and <button>CTRL</button>+<button>-</button> shortcuts, in case the font is too small or large. You can also adjust the JupyterLab content or UI font size specifically from the <b>Settings -> JupyterLab Theme -> Increase/Decrease</b> relevant font size options.</p>\n",
    "\n",
    "<p><b>Commands</b>: Commands will usually be formatted as the following: <code>command [input 1] [input 2] ...</code>. This means you should type \"command\" into the terminal, followed by an additional one or more user-supplied inputs separated by spaces (the <code>...</code> indicates that more arguments could be added but are not shown). Square brackets and ellipses should not be included in the typed command.</p>\n",
    "\n",
    "<p>Lines starting with a \"<span class=\"centered\" style=\"font-size:20px\">⮚</span>\" symbol indicate that what follows is a command that you should run in the terminal.</p>\n",
    "\n",
    "<p><b>Keyboard shortcuts</b>: Shortcuts (and key press combinations) will be displayed using a key symbol. E.g. <button>ENTER</button> means \"press the enter key\".</p>\n",
    "\n",
    "<p><b>Files, directories and paths</b> will be in blue: <span class=\"path\">file.txt</span>, <span class=\"path\">directory</span>, <span class=\"path\">/home/user/</span></p>\n",
    "\n",
    "<p><b>Blue, orange and green boxes</b> can be expanded by clicking the box. Orange boxes contain additional information related to the workshop contents and blue boxes contain the answers to posed questions or excercises. Green boxes contain \"advanced\" information or exercises (relative to the current section of work) - if they are too complex, you can always return to them at the end of the day or on another day.</p>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"details-container\">\n",
    "<details>\n",
    "<summary class=\"info\">Click here to reveal additional information.</summary>\n",
    "<div class=\"answer info\">\n",
    "\t<span>Additional information.<i/></span><br>\n",
    "</div>\n",
    "</details>\n",
    "</div>\n",
    "\n",
    "\n",
    "<div class=\"details-container\">\n",
    "<details>\n",
    "<summary class=\"\">This is a question or task.</summary>\n",
    "<div class=\"answer\">\n",
    "\t<span>Answer or explanation.</span><br>\n",
    "</div>\n",
    "</details>\n",
    "</div>\n",
    "\n",
    "<div class=\"details-container\">\n",
    "<details>\n",
    "<summary class=\"advanced\">This is advanced information or an advanced exercise.</summary>\n",
    "<div class=\"answer advanced\">\n",
    "\t<span>More advanced information about the current topic.</span><br>\n",
    "</div>\n",
    "</details>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "## Kernels & Code cells\n",
    "\n",
    "_**Note: this section is only relevant for Day 4/Day 5, so you may skip it if reading at the start of the workshop.**_\n",
    "\n",
    "The final sections of the workshop will be run using R in executable JupyterLab code cells. Here is a short list of general information on code cells.\n",
    "\n",
    "**Kernel:** Code execution can only occur in a JupyterLab notebook if it attached to an active kernel session. You can see whether a kernel is present at the top right corner of the notebook; it should say \"Python 3\" if a kernel is active, or \"No Kernel!\" if one is not active. Click the \"No Kernel!\" text to bring up a box to start a new kernel.\n",
    "\n",
    "**Kernel Sessions:** The kernel is the computing environment in which your code will run. If the kernel crashes or restarts then all of stored data (variables, loaded packages, etc.) will disappear. Unfortunately, Binder kernels will automatically shut down after a period of inactivity - so try do the hands-on sections in one sitting, if possible.\n",
    "\n",
    "**Kernel busy**: The circle next to the kernel indicator at the top right of the notebook indicates whether the kernel is currently busy (running a command). A white circle means it is idle, a shaded circle meas it is \"busy\". A \"busy\" kernel will not run any other commands until is has completed its task.\n",
    "\n",
    "**No code cell output**: If you are expecting output, then it is probably because the kernel is not present. Check the top right of the notebook. Alternatively, the notebook kernel may be busy processing a previous command - check the colour of the circle next to the kernel (shaded means it is busy, empty means it is idle).\n",
    "\n",
    "**\"UsageError: Cell magic `%%R` not found\"**: This error means that the `%load_ext rpy2.ipython` command has not been run (see below).\n",
    "\n",
    "**Code cells:** Code cells can be used to execute code on the current kernel. The default interpreter (and kernel) is Python 3. Code can be executed by selecting the cell and pressing <button>CTRL</button>+<button>ENTER</button>. The code output (if there is any) will be displayed underneath the code cell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'This is a Python command'"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "# this is a code cell; select and press CTRL+ENTER to run\n",
    "# make sure the kernel is active!\n",
    "# the output will be printed below this cell\n",
    "display(\"This is a Python command\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "**R cell magic**: this workshop will primarily use code cells to execute R code. In order to run R, two things must be done:\n",
    "- the `rpy2` package must first be loaded in the notebook kernel\n",
    "- any R code must be in a code cell that starts with `%%R`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "# this is a Python code cell\n",
    "# load external package rpy2.ipython\n",
    "# this need only be done once in the notebook (unless the kernel resets)\n",
    "%load_ext rpy2.ipython"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] \"This is an R command\"\n"
     ]
    }
   ],
   "source": [
    "%%R\n",
    "# this is an R code cell\n",
    "# all code in this cell will be interpreted as R, since the first line is %%R\n",
    "print(\"This is an R command\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "## R Overview\n",
    "\n",
    "_**Note: this section is only relevant for Day 4/Day 5, so you may skip it if reading at the start of the workshop.**_\n",
    "\n",
    "Don't forget that all R code cells must start with `%%R`!\n",
    "\n",
    "You can use the cell below if you want to experiment."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# run this once to allow R code to be executed\n",
    "%load_ext rpy2.ipython"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "# paste example code here to test"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "# paste example code here to test"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "**R commands**: Arguments are supplied to R functions (commands) inside brackets. Most R commands require objects (variables) as input, rather than working on files as in Bash.\n",
    "\n",
    "```bash\n",
    "# example Bash command: look at top 20 lines of file\n",
    "head -n 20 file.txt\n",
    "```\n",
    "\n",
    "```R\n",
    "# example R command: look at top 20 lines of object\n",
    "head(data,20)\n",
    "```\n",
    "\n",
    "```R\n",
    "# some arguments include specific flags such as \"file=\" or \"sep=\" (depending on the function)\n",
    "read.table(file=\"file.txt\",header=TRUE,sep=\";\")\n",
    "```\n",
    "\n",
    "**Loading packages**: To load an R package into the current environment, use the `library()` function:\n",
    "\n",
    "```R\n",
    "# load package \"DESeq2\" (quoted) into current environment so that you can run it\n",
    "library(\"DESeq2\")\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "**Lists**: Use the `c()` function to create lists:\n",
    "\n",
    "```bash\n",
    "# example Bash list\n",
    "# first element has index 0\n",
    "list=( \"A\" \"B\" \"C\" )\n",
    "echo ${list[0]}\n",
    "```\n",
    "\n",
    "```R\n",
    "# example R list\n",
    "# each argument to c will be one element in the list\n",
    "# first element has index 1\n",
    "list=c(\"A\",\"B\",\"C\")\n",
    "print(list[1])\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "**Combine text strings**: The `paste()` function can be used to join text strings together.\n",
    "\n",
    "```R\n",
    "# R paste command\n",
    "# Output will be a string, or a list of strings (if any of the inputs were lists)\n",
    "paste(\"left\",\"middle\",\"right\",sep=\"|\")\n",
    "paste(c(\"A\",\"B\",\"C\"),c(\"X\",\"Y\",\"Z\"),sep=\"|\")\n",
    "```\n",
    "\n",
    "```R\n",
    "# paste0 will join text without any delimiter\n",
    "paste(\"A\",\"B\",\"C\") # outputs \"A B C\"\n",
    "paste(\"A\",\"B\",\"C\",sep=\",\") # outputs \"A,B,C\"\n",
    "paste(\"A\",\"B\",\"C\",sep=\"\") # outputs \"ABC\"\n",
    "paste0(\"A\",\"B\",\"C\") # outputs \"ABC\"\n",
    "```\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "**Repeating text strings**: The `rep()` command can be used to repeat a given string (or list of strings) a set number of times. This can be useful when creating sample tables, for example.\n",
    "\n",
    "```R\n",
    "# rep using the \"times\" argument\n",
    "rep(c(\"A\",\"B\",\"C\"),times=3) # outputs \"A B C A B C A B C\"\n",
    "```\n",
    "\n",
    "```R\n",
    "# rep using the \"each\" argument\n",
    "rep(c(\"A\",\"B\",\"C\"),each=3) # outputs \"A A A B B B C C C\"\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "**Reading data from a file**: The `read.table()` function can be used to import data from a text file.\n",
    "\n",
    "```R\n",
    "# import data from a text file\n",
    "# default delimiter is tab, change using \"sep=\" argument\n",
    "# header: TRUE if the file has a header row, FALSE otherwise\n",
    "# row.names: give the column number of the column that contains the row names (e.g. gene names) if necessary\n",
    "read.table(file=\"file.txt\",sep=\"[delimiter]\",(header=TRUE/FALSE),(row.names=[column]))\n",
    "```\n",
    "\n",
    "```R\n",
    "# same as table, but comma is default delimiter\n",
    "read.csv(file=\"file.txt\",sep=\"[delimiter]\",(header=TRUE/FALSE),(row.names=[column]))\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "**Create a dataframe from lists**: This is one way of creating an R dataframe (2D table of data) using lists as input:\n",
    "\n",
    "```R\n",
    "# column1, column2 etc. will be the column name in the dataframe\n",
    "# list1, list2 etc. will be the column data in the dataframe\n",
    "do.call(rbind,Map(data.frame,[column1]=factor([list1]),[column2]=factor([list2]),...,[columnN]=factor([listN])))\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "**Subsetting a dataframe**: There are several ways of accessing column or row data from a dataframe. These are some examples.\n",
    "\n",
    "```R\n",
    "# columns\n",
    "dataframe$column_name\n",
    "dataframe[[\"column_name\"]]\n",
    "dataframe[,\"column_name\"]\n",
    "dataframe[,2] # column 2\n",
    "```\n",
    "\n",
    "```R\n",
    "# rows\n",
    "dataframe[\"row_name\",]\n",
    "dataframe[1,] # row 1\n",
    "```\n",
    "\n",
    "```R\n",
    "# specific value (column x row)\n",
    "dataframe[\"row_name\",\"column_name\"]\n",
    "dataframe[3,2]\n",
    "```"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
