<div align="center">Introduction to Next Generation Sequencing<br>2021</div>

# Introduction to Next Generation Sequencing

This workshop will be run remotely to comply with social distancing measures. You will be able to run through this module yourself and visit a workshop <a href="https://slack.com/">Slack</a> channel for real-time questions or discussion.

This module consists of course notes and data files stored in a <a href="https://en.wikipedia.org/wiki/Git">git</a> repository that have been hosted (for free) on a <a href="https://jupyter.org">JupyterLab/JupyterHub</a> server provided by <a href="https://mybinder.org/">Binder</a>. Knowledge on how to setup or use this software is not relevant to the contents of the course, but if you are interested in more information about git repositories, the Jupyter Project or Binder you can ask on Slack.

When you are ready, click the Binder badge below to launch your personal temporary server. It may take a few minutes to load.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/rafelyall%2Fintroduction-to-ngs/master?urlpath=lab/workspaces/contents)
