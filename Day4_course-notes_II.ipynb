{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<header style=\"text-align:center;color:darkgray\">Introduction to Next Generation Sequencing</header>\n",
    "<header style=\"text-align:center;color:darkgray\">2021</header>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "<h1>Day 4B: An Introduction to R</h1>\n",
    "\n",
    "<p>The downstream analyses you perform tomorrow will be done in R. Although this workshop does not have enough time to give a thorough overview of the R coding language, it will be necessary to at least go over a few simple concepts. You can find additional links and information on the <a target=\"_parent\" href=\"https://www.r-project.org/\">R project website</a>; many other instructional resources can also be found online. </p>\n",
    "\n",
    "<p><b>Don't forget you can drag the borders of the bottom section to enlarge it. You will not be using the terminal for this section.</b></p>\n",
    "\n",
    "<p>Often an R analysis would not be performed on the command line but using a dedicated R environment, such as <a target=\"_parent\" href=\"https://rstudio.com/\">RStudio</a>. However, another option is to use JupyterLab, the environment that this workshop is built on. JupyterLab is a browser-based <i>computational</i> environment and the JupyterLab Notebook (this document) can be used to mingle both code and text. Individual notebook sections (\"cells\") can be used to write and execute code, including R. This functionality will be used in the remaining workshop sections to perform an analysis of RNA-Seq datasets to identify targets of the Arabidopsis AINTEGUMENTA transcription factor (Krizek et al 2020).</p>\n",
    "\n",
    "<p>The use of code cells and a brief introduction to R will be given as the last section today, whereas the analysis will be performed tomorrow. A refresher of how to use code cells, the R cell magic and R commands is also given at the end of the Help file (which should be loaded in every workspace). You can use this if you want a quick reminder on how to do specific tasks tomorrow.</p>\n",
    "\n",
    "<h3>Code cells</h3>\n",
    "\n",
    "<p>Jupyter notebooks consist of two types of cells - markdown cells, used for text, and code cells, used to execute code. The following cell is a <i>code cell</i> and can be executed by selecting it and pressing <button>CTRL</button>+<button>ENTER</button>. When it is run it will print any output below the cell. Try it below:</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# this is a code cell running Python, execute it using CTRL+ENTER\n",
    "display(\"Hello world!\")\n",
    "\n",
    "\n",
    "# if it does not work, continue reading below\n",
    "# you can edit the cell contents yourself and rerun it if you want"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "<p>More information about code (and markdown) cells can be found in the JupyterLab <a target=\"_parent\" href=\"https://jupyterlab.readthedocs.io/\">documentation</a>. To add your own code cells, you can use the the <button>A</button> or <button>B</button> keys to insert a cell above or below the currently selected cell, respectively (they will appear empty).</p>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "<h3>Kernels</h3>\n",
    "\n",
    "<p>In order for code execution to work, the notebook must be connected to an active \"kernel\". This kernel is the environment in which code will be interpreted and run. You can check if a notebook is currently connected to a kernel at the top right of the toolbar above each notebook: it should have an empty circle with either the text \"Python 3\" or \"No Kernel!\". If the code above worked, then your notebook is already connected to the kernel and should say \"Python 3\":</p>\n",
    "\n",
    "<img src=\"./public/kernel_python.png\" style=\"width:12%\">\n",
    "\n",
    "<p>If it did not run, then your indicator will likely say \"No kernel!\" instead:</p>\n",
    "\n",
    "<img src=\"./public/no_kernel_v2.png\" style=\"width:12%\">\n",
    "\n",
    "<p>If the notification says \"No Kernel!\", simply click the text to bring up a box to start a new kernel:</p>\n",
    "\n",
    "<img src=\"./public/select_kernel_v2.png\" width=\"400px\">\n",
    "\n",
    "<p>The small circle next to the kernel will be shaded if the kernel is currently executing code and empty otherwise. The JupyterLab browser tab icon will also become an hourglass. While it is shaded (busy) you will be unable to execute further code until it has finished.</p>\n",
    "\n",
    "<b>In the case that your kernel disconnects: If the Kernel disconnects, you will lose all saved variables and loaded programs. You can rerun all of the cells above a certain point by using the \"Run All Above Selected Cell\" option in the \"Run\" menu at the top of the screen. Simply navigate to where you were in the notebook and select this option; it will automatically run all cells above that cell in order.</b>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "<h3>Running R in JupyterLab</h3>\n",
    "\n",
    "<p>The notebooks in this workshop are set up to use the Python 3 kernel, though they can be used with dozens of either kernel types associated with other languages (Julia, Ruby, Perl, Bash, R...). However, even though it is based on Python, it can actually still interpret and run R code provided the <code>rpy2</code> module is loaded. This module only needs to be loaded once in the same environment (kernel session), though if the kernel restarts you will need to re-run it. Run the code cell below to activate R in this notebook:</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# run this cell to activate R; make sure a kernel is active\n",
    "# you only need to do this once per kernel session\n",
    "%load_ext rpy2.ipython\n",
    "\n",
    "# change directory\n",
    "%cd day4_exercises\n",
    "\n",
    "# view directory contents\n",
    "%ls"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "<p>Once <code>rpy2</code> is loaded you will have access to R. To use R code the first line of the cell needs to be <code>%%R</code>. You can think of this as similar to a shebang line in Bash - it will tell the kernel to execute this cell as R rather than Python. Try it in the cell below, which will print \"Hello world!\" using the R <code>print()</code> function:</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "# this cell is interpreted as R and will run R commands\n",
    "# print is the R version of Bash echo\n",
    "print(\"Hello world!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "<p>Any variables you define in an R cell anywhere within a notebook will be \"visible\" to all other R cells, so you can spread code out and interperse code cells with text or comments:</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "# import data from the R mtcars dataset, which contains some car-based data\n",
    "print(mtcars)\n",
    "wt=mtcars$wt # car weights\n",
    "mpg=mtcars$mpg # car miles per gallon"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "<p>Code cells also allow you to include plots directly in the notebook, as they will be displayed below the cell:</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "# plot weight versus MPG\n",
    "plot(wt,mpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "<p>As expected, there is a negative correlation between weight and fuel efficiency.</p>\n",
    "\n",
    "<p>For the analysis tomorrow most of the cells will be prepared for you, and you will only need to execute them.</p>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "# `R`: A Brief Introduction\n",
    "\n",
    "Most of the common packages that perform downstream statistical analysis of NGS data are written to run in R. As such it is important to know at least a base amount of R in order to sufficiently perform these steps of your analysis. The following sections will serve to explain the basics of working in R and the commands that will be used in the workshop tomorrow."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "### R commands\n",
    "\n",
    "In Bash, commands have the following structure:\n",
    "\n",
    "```bash\n",
    "command arg1 arg2 agr3 # positional arguments\n",
    "command -p 4 --file file.txt --sep \",\" # keyword argument (-p, --file, --sep)\n",
    "```\n",
    "\n",
    "In R, the equivalent commands/functions consist of `command(arg1,arg2,arg3)`:\n",
    "\n",
    "```R\n",
    "command(arg1,arg2, ... ,argN)\n",
    "```\n",
    "They may be positional (i.e. the first, second or third arguments must have specific values, as above) or each argument might require a specific flag, such as `file=` or `sep=`:\n",
    "\n",
    "```R\n",
    "command(file=\"file.txt\",sep=\",\", ... , z=argN)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "### Installing packages\n",
    "\n",
    "Installing packages in R uses the `install.packages()` function, where the name of the package to be installed is placed inside the brackets. This function must be run from the R environment:\n",
    "\n",
    "```R\n",
    "install.packages(tximport)\n",
    "```\n",
    "\n",
    "This workshop, however, is run using an Anaconda virtual environment. Most of the mainstream R packages also have Anaconda-based install options using `conda install`, which must be done from the command line:\n",
    "\n",
    "```R\n",
    "conda install -y -c bioconda bioconductor-tximport\n",
    "```\n",
    "\n",
    "All the necessary R packages used in the following section(s) have already been installed on the workshop environment, however, so this should not be necessary.\n",
    "\n",
    "### Help\n",
    "\n",
    "To get help with an R function or command, simply type `?command` into an R cell (or look for the documentation online):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "# to hide the cell output, click the blue bar to the left of the output (this works for input cells as well)\n",
    "# to unhide, simply click the ellipsis that appears in its place\n",
    "?install.packages"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "### Loading packages\n",
    "\n",
    "Once a package has been installed you must load it into the current R session before it can be used. This is done using the `library(\"package\")` function, which takes the package name as an argument. There will be no indication that the command has run successfully, but it will print an error if it fails to load the package.\n",
    "\n",
    "_When working in JupyterLab, the R session is the lifetime of the current kernel - if the kernel restarts you will need to reload all packages._"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "# load the tximport library into the current R session\n",
    "library(\"tximport\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "### Lists\n",
    "\n",
    "The `tximport` library will be explained in further detail in the DGE analysis section tomorrow. For now, it is necessary to know that it requires a list of _paths to alignment output files_ as one of its arguments. For this analysis, that means the <span class=\"path\">quant.sf</span> output files from Salmon.\n",
    "\n",
    "In Bash, you would create a list (indexed array) using the `list=( element1 element2 element3 )` syntax:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "# the %%bash \"shebang\" tells the kernel to interpret this cell as Bash\n",
    "my_list=( \"file1.txt\" \"file2.txt\" \"file3.txt\" )\n",
    "echo \"The first element in the list is '${my_list[0]}'\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "In R, the `c()` function is used to combine a group of values into a list or vector. The list of elements to be added are provided as comma-separated arguments:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "# the list should get printed below the cell when you run this\n",
    "c(\"file1.txt\",\"file2.txt\",\"file3.txt\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "Unlike Bash, the key of the first element in a list is 1, not 0:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "my_list=c(\"file1.txt\",\"file2.txt\",\"file3.txt\")\n",
    "print(my_list[1])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "#### **_`paste()` & `rep()`_**\n",
    "\n",
    "If you had run an alignment using all six read files, the Salmon output <span class=\"path\">quant.sf</span> files would be found in one of six different directories which follow the pattern `8hr_[sample-type]_[replicate-number}`:\n",
    "\n",
    "```R\n",
    "8hr_control_1/quant.sf\n",
    "8hr_control_2/quant.sf\n",
    "8hr_control_3/quant.sf\n",
    "8hr_treated_1/quant.sf\n",
    "8hr_treated_2/quant.sf\n",
    "8hr_treated_3/quant.sf\n",
    "```\n",
    "\n",
    "Obviously, it would be simple to just create the list of file paths by copying each path into a `c()` function by hand . However, it provides a helpful example to introduce some other R commands: `paste()` and `rep()`.\n",
    "\n",
    "**`paste()`**\n",
    "\n",
    "The `paste()` function is used to combine each of its arguments using a delimiter (_separator_) given by the `sep=` argument (default space):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "paste(\"left\",\"middle\",\"right\",sep=\"|\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "In the above example, it produces a single string by concatenating the \"left\", \"middle\" and \"right\" text arguments using the \"|\" delimiter. The `paste()` command can also take lists as arguments, in which case it will combine the _contents_ of the lists: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "left=c(\"A\",\"B\",\"C\")\n",
    "right=c(\"X\",\"Y\",\"Z\")\n",
    "paste(left,right,sep=\"|\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Each element in the `left` list is combined with the equivalent element in the `right` list."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "<div class=\"details-container\">\n",
    "<details>\n",
    "<summary class=\"\">What happens if the lists are different lengths (edit the previous example)?</summary>\n",
    "<div class=\"answer\">\n",
    "<span>The shorter list will loop back to the beginning so that, for example, \"A\" will be combined with the element that comes after \"Z\" in the previous example.</span>\n",
    "</div>\n",
    "</details>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "The `paste0()` function is the same as the `paste()` function, but by default its separator is an empty string. It can be used to quickly combine strings without using a separator: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "paste0(\"8hr_\",\"control_1\",\"/\",\"quant.sf\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "**`rep()`**\n",
    "\n",
    "The `rep()` function is used to repeat a set of values a given number of times:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "rep(\"A\",times=3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "rep(\"A\",times=10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "As with `paste()`, the values can also be a list:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "rep(c(\"A\",\"B\",\"C\"),times=3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "The `times=` argument will repeat the entire list a given number of times (as above). The `each=` argument will repeat each _element_ in the list a given number of times (note the difference in how the values are ordered):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "rep(c(\"A\",\"B\",\"C\"),each=3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "You can combine the `paste()`/`paste0()` and `rep()` commands to quickly produce sample lists/tables that have samples with similar names that differ only in type of treatments, species, genotype, sampling time or replicates - something that commonly occurs in NGS-related projects.\n",
    "\n",
    "```R\n",
    "# example\n",
    "8hr_control_1/quant.sf\n",
    "8hr_control_2/quant.sf\n",
    "8hr_control_3/quant.sf\n",
    "8hr_treated_1/quant.sf\n",
    "8hr_treated_2/quant.sf\n",
    "8hr_treated_3/quant.sf\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"details-container\">\n",
    "<details>\n",
    "<summary class=\"\">Modify the cell below to create a list of the paths to each <span class=\"path\">quant.sf</span> file (assuming you had run on all six files)</summary>\n",
    "<div class=\"answer\">\n",
    "    <code>quantfiles=paste0(\"8hr_\",rep(sample_types,each=3),\"_\",rep(rep_number,times=2),\"/quant.sf\")</code><br><br>\n",
    "\t<span>Assign this list to a variable <code>quantfiles</code> so that we can use it later.</span>\n",
    "</div>\n",
    "</details>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "# create a list that contains the relative path to each quant.sf file\n",
    "sample_types=c(\"control\",\"treated\")\n",
    "rep_number=c(\"1\",\"2\",\"3\")\n",
    "# insert your code below\n",
    "quantfiles="
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "You can check whether the file paths you have created are correct by supplying the list to the `file.exists()` function. It will return TRUE if a file exists with the given path, or FALSE otherwise. The command below should return TRUE once (for the first sample), and FALSE for the remaining samples you have not run:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "# this will only print TRUE is Salmon has finished running\n",
    "print(file.exists(quantfiles))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "### Reading data from a file\n",
    "\n",
    "The `tximport` package needs additional data: the list of transcript-to-gene correspondence you created above. Since this is stored in a text file, you will need to import the data into R.\n",
    "\n",
    "You can import data into R using the `read.table()` function (and related functions, such as `read.csv()`). These functions have the following general form:\n",
    "\n",
    "```R\n",
    "read.table(file=\"[file.txt]\",sep=\"[delimiter]\",(header=[TRUE/FALSE]),(row.names=[row names column]))\n",
    "```\n",
    "\n",
    "The function will read the contents of a given file (provided by the first argument, or the `file=` argument) and separate columns based on the value supplied by the `sep=` argument (default is tab). The `header=` and `row.names=` flags are optional, and tell the function whether the first line should be treated as a header (rather than data) and what row should be used to infer row names (again, rather than treating it as data)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"details-container\">\n",
    "<details>\n",
    "<summary class=\"\">Import the data from the <span class=\"path\">transcript-to-gene.txt</span> file into R as the <code>tx2gene</code> variable below</summary>\n",
    "<div class=\"answer\">\n",
    "<code>tx2gene=read.table(\"transcript-to-gene.txt\",header=TRUE)</code><br><br>\n",
    "\t\t<span>The file is tab-delimited so the <code>sep=</code> argument does not need to be set. The first line is the header (\"transcript\" and \"gene\"), but there are no row names.</span>\n",
    "</div>\n",
    "</details>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "# import transcript-to-gene files as the \"tx2gene\" object (variable)\n",
    "tx2gene="
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "Similar to Bash, R has a `head()` function:\n",
    "\n",
    "```R\n",
    "head(data,num_lines)\n",
    "```\n",
    "\n",
    "You can use this to view the top lines of a data file, such as the `tx2gene` data table:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "head(tx2gene,10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "### Data Frames\n",
    "\n",
    "Data frames are a central part of doing data anlaysis in R. Put simply, they are two-dimensional tables of data divided into rows and columns. Columns must be named and row names must be unique. There is not sufficient time to go into detail about data frames and how to use or manipulate them in R. However, as the programs in the next section require dataframes for certain aspects of their analysis, it would be useful to at least introduce a way to create a dataframe.\n",
    "\n",
    "One method - which will be useful in this workshop - is to create a dataframe using lists, such that each list will be a column in the final data frame:\n",
    "\n",
    "```R\n",
    "do.call(rbind,Map(data.frame,[column1]=factor([list1]),[column2]=factor([list2]),...,[columnN]=factor([listN])))\n",
    "```\n",
    "\n",
    "If you are interested, I suggest looking up the various subcommands yourself.\n",
    "\n",
    "Using this command, you can create a sample table (as a dataframe) that represents each of your samples and whether their \"condition\" (whether they are treated/control samples):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "# create list of sample IDs\n",
    "samples=paste0(\"8hr_\",rep(c(\"treated\",\"control\"),each=3),rep(c(\"_1\",\"_2\",\"_3\"),times=2))\n",
    "samples"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "# create list of sample conditions (treated/control)\n",
    "conditions=rep(c(\"treated\",\"control\"),each=3)\n",
    "conditions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "# create a sample table as a dataframe\n",
    "sample_table=do.call(rbind,Map(data.frame,sample=factor(samples),condition=factor(conditions)))\n",
    "sample_table"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "You can view specific columns or rows in a dataframe by using one of several methods, outlined below. There are also more advanced ways of subsetting row/columns using these methods, but this workshop will go into no further details. \n",
    "\n",
    "To view columns, you can either specify the column name or the column number:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "# view column \"condition\" (method 1)\n",
    "sample_table$condition"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "# view column \"condition\" (method 2)\n",
    "sample_table[[\"condition\"]]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "# view column \"condition\" (method 3)\n",
    "sample_table[,\"condition\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "# view the 2nd column\n",
    "sample_table[,2]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "Rows can also be extracted using either the row position or row name (note that in this example, the row names are _also_ numbers, but they are strings not numeric):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "# extract the row NAMED \"1\"\n",
    "sample_table[\"1\",]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "# extract the row at position 1\n",
    "sample_table[1,]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "# view row 3, column 2\n",
    "sample_table[3,2]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "### Conclusion\n",
    "\n",
    "There is obviously far, far more to working efficiently in R. However, this brief introduction should suffice to let you understand the commands used in the analyses tomorrow."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
