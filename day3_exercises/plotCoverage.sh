#!/bin/bash

# restrict view to several thousand basepairs up- and down-stream the AP1 gene region
pyGenomeTracks --tracks .track.ini --region Chr1:25,980,000-25,990,000 -o AP1_coverage.png
